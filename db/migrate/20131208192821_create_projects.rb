class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :lecturer
      t.datetime :creationDate

      t.timestamps
    end
  end
end
