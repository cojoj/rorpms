class RemoveCreationDate < ActiveRecord::Migration
  def self.up
    remove_column :projects, :creationDate
  end
end
