class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.references :project, index: true

      t.timestamps
    end
  end
end
