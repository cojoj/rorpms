class ProjectsController < ApplicationController
  def index
  end
  
  def show
    @projects = Project.all
  end
  
  def create
    @project = Project.new(project_params)
    @project.status = "Waiting"
    @project.save
    redirect_to root_path
  end
  
  def new
    @project = Project.new(params[:id])
    @project.status = "Waiting"
    @project.save
    redirect_to root_path
  end
  
  private
    def project_params
      params.require(:project).permit(:title, :lecturer)
    end
end
